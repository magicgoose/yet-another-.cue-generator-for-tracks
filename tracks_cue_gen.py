#!/usr/bin/env python3
import codecs
import itertools
import os
import re
import subprocess as sp
import sys
from collections import defaultdict
from os import path
from typing import NamedTuple

from natural_sort import natural_sort_key

ex_ffprobe = 'ffprobe'
ex_ffmpeg = 'ffmpeg'

known_audio_extensions = {
    'aac',
    'ac3',
    'aif',
    'aiff',
    'ape',
    'dts',
    'flac',
    'm4a',
    'mka',
    'mp2',
    'mp3',
    'mpc',
    'ofr',
    'ogg',
    'opus',
    'tak',
    'tta',
    'wav',
    'wv',
}


class Track(NamedTuple):
    filename: str
    album: str
    artist: str
    title: str
    date: str
    genre: str


def _track(directory, filename):
    file_path = path.join(directory, filename)

    p = sp.Popen(
        (ex_ffprobe,
         '-v', 'error',
         '-select_streams', '0:a:0',
         '-show_entries', 'format_tags=title,artist,album,date,genre',
         file_path),
        stdout=sp.PIPE)
    out, err = p.communicate()
    returncode = p.returncode
    if returncode != 0:
        raise Exception('ffprobe returned {}'.format(returncode))
    ffprobe_output = out.decode('utf-8')

    d = defaultdict(lambda: None)
    for m in re.finditer(r'([a-zA-Z_:]+)=(.+)', ffprobe_output):
        v = m.groups()
        d[v[0].lower()] = v[1]

    return Track(
        filename,
        d['tag:album'],
        d['tag:artist'],
        d['tag:title'],
        d['tag:date'],
        d['tag:genre'],
    )


def _get_tracks(work_dir):
    for dirpath, dirnames, filenames in os.walk(work_dir, topdown=True):
        filenames = sorted(filenames, key=natural_sort_key)
        for f in filenames:
            _, ext = path.splitext(f)
            ext = ext[1:].lower()
            if ext in known_audio_extensions:
                yield _track(work_dir, f)
        break


def unique(iterable):
    seen = set()
    for x in iterable:
        if x not in seen:
            yield x
            seen.add(x)


def generate_cue(work_dir):
    tracks = tuple(_get_tracks(work_dir))

    def grouped_all(sep, field):
        unique_items = tuple(unique(filter(None, (getattr(t, field) for t in tracks))))
        if not unique_items:
            return None
        return sep.join(unique_items)

    album = grouped_all(' / ', 'album')
    artist = grouped_all(' / ', 'artist')
    genre = grouped_all(', ', 'genre')
    date = grouped_all(', ', 'date')

    if genre:
        yield f'REM GENRE {genre}\n'
    if date:
        yield f'REM DATE {date}\n'
    if artist:
        yield f'PERFORMER "{artist}"\n'
    if album:
        yield f'TITLE "{album}"\n'

    for track, track_number in zip(tracks, itertools.count(1)):
        yield f'FILE "{track.filename}" WAVE\n'
        yield f'  TRACK {track_number:02} AUDIO\n'
        track_title = track.title or track.filename
        yield f'    TITLE "{track_title}"\n'
        yield f'    INDEX 01 00:00:00\n'


def main():
    work_dir = sys.argv[1] if len(sys.argv) > 1 else os.getcwd()
    cue = generate_cue(work_dir)
    cue_output_path = path.join(work_dir, '_tracks.cue')
    with open(cue_output_path, 'wb') as f:
        f.write(codecs.BOM_UTF8)
        for line in cue:
            f.write(line.encode())
    print(f'Wrote {cue_output_path}')


if __name__ == '__main__':
    main()
